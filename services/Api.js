import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'https://smartgoalstraining.com/skillgames-api/'
  })
}
