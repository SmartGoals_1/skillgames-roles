import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import Api from './Api'

Vue.use(Vuex)
//Vue.prototype.$APIlink= 'https://smartgoalstraining.com/skillgames-api-v2/'
Vue.prototype.$FullLogo= "https://smartgoalstraining.com/skillgames-api/online-img/logo.png"
// Vue.prototype.$APIlink= 'http://laravel.fieldback.net/api/'
Vue.prototype.$APIlink= 'https://api.skillgam.es/api/'
//Vue.prototype.$APIlink= 'https://smartgoalstraining.com/skillgames-api-v2/'


Vue.config.productionTip = false
const LOGIN = "LOGIN";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGOUT = "LOGOUT";
var token = new FormData();
token.set('token', sessionStorage.getItem('token'))
const store = new Vuex.Store({
  state: {
    isLoggedIn: !!sessionStorage.getItem('token'),
    isLoading: false,
    currentUser: {},
 
  },
  mutations: {
    [LOGIN] (state) {
      state.pending = true;
    },
    [LOGIN_SUCCESS] (state) {
      state.isLoggedIn = true;
      state.pending = false;
    },
    [LOGOUT](state) {
      state.isLoggedIn = false;
    },
    IS_LOADING (state, payload) {
      state.isLoading = payload
    },
    GET_USER (state, payload) {
      state.currentUser = payload
    },
  },
  actions: {
    login({ commit }, token) {
      commit(LOGIN);
      return new Promise(resolve => {
        setTimeout(() => {
          if(!sessionStorage.getItem('token')){
            sessionStorage.setItem('token', token.token);
            commit(LOGIN_SUCCESS);
            resolve();
          }
         
        }, 1000);
      });
    },
    logout({ commit }) {
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('name');
      sessionStorage.removeItem('image');
      sessionStorage.removeItem('role');
      sessionStorage.removeItem('number');
      sessionStorage.removeItem('id');
      sessionStorage.removeItem('organisation');
      sessionStorage.removeItem('organisation_name');
      sessionStorage.removeItem("team_id")


      commit(LOGOUT);
    },
    GET_USER_INFO: async ({commit}) => {
      commit('IS_LOADING', true) 
      const { data } = await Api().post('getUserInfo', token)
      commit('GET_USER', data.players[0])
      commit('IS_LOADING', false)
    },
    // UPDATE_USER: async ({commit, dispatch}, {user_id: id, first_name: fn, last_name: ln, age: a,height: h, weight: w, teams: t }) => {
    //   //commit('IS_LOADING', true) 
    //   //console.log("Update object", {user_id: id, first_name: fn, last_name: ln, age: a,height: h, weight: w, teams: t })
    //   const { data } = await Api().post('updateUser', {token: sessionStorage.getItem('token'), user_id: id, first_name: fn, last_name: ln, age: a, height: h, weight:w, teams:t })
    // },

  },
  getters: {
    isLoggedIn: state => state.isLoggedIn,
    currentUser: state=> state.currentUser,
  }
});  
/* eslint-disable no-new */

new Vue({ el: '#app', router, store, components:{App}, render: h => h(App) })