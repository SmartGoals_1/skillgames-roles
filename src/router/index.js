import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/components/LogIn'
import Profile from '@/components/Profile'
import IndividualAchievements from '@/components/Individual/IndividualAchievements'
import IndividualRoute from '@/components/Individual/IndividualRoute'
import IndividualAchievementsSkillGame from '@/components/Individual/IndividualAchievementsSkillGame'
import Dashboard from '@/components/Trainer/Dashboard'
import Players from '@/components/Trainer/Players'
import PlayersRoute from '@/components/Trainer/PlayersRoute'
import PlayerAchievementsSkillGame from '@/components/Trainer/PlayerAchievementsSkillGame'
import PlayerAchievements from '@/components/Trainer/PlayerAchievements'
import Teams from '@/components/Trainer/Teams'
import TeamRoute from '@/components/Trainer/TeamRoute'
import TeamAchievements from '@/components/Trainer/TeamAchievements'
import TeamAchievementsListGrid from '@/components/Trainer/TeamAchievementsListGrid'
import TeamAchievementsGrid from '@/components/Trainer/TeamAchievementsGrid'
import Sessions from '@/components/Trainer/Sessions'
import SessionsRoute from '@/components/Trainer/SessionsRoute'
import SingleSession from '@/components/Trainer/SingleSession'
import DataSkillGamesRouter from '@/components/Trainer/DataSkillGamesRouter'
import DataSkillGames from '@/components/Trainer/DataSkillGames'
import DataSkillGamesAchievements from '@/components/Trainer/DataSkillGamesAchievements'
import OverallData from '@/components/Trainer/OverallData'
import LiveScoreboard from '@/components/Trainer/Livescoreboard'
import ManagePlayers from '@/components/Management/ManagePlayers'
import ManageSkillGames from '@/components/Management/ManageSkillGames'
import ManageTeams from '@/components/Management/ManageTeam'
import ManageTrainers from '@/components/Management/ManageTrainers'
import ManageOrganisations from '@/components/Management/ManageOrganisations'
import ManageUsers from '@/components/Management/ManageUsers'
import Register from '@/components/Register'

Vue.use(Router) 
const router = new Router({
  // mode: 'history',
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: LogIn
    },
    {
      path: '/login',
      name: 'Login',
      component: LogIn
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/general',
      name: 'OverallData',
      component: OverallData,
    },
    {
    path: '/individual',
      component: IndividualRoute,

      children: [
        {
          path: '',
          name: 'IndividualAchievements',
          component: IndividualAchievements

        },
        {
          path: 'skillgame/:id/:pname/:sgname/:sgid',
          props: true,
          name: 'Individual Achievements Skill Game',
          component: IndividualAchievementsSkillGame,
        }
      ]
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/manageskillgames',
      name: 'ManageSkillGames',
      component: ManageSkillGames,
    },
    {
      path: '/manageorganisations',
      name: 'ManageOrganisations',
      component: ManageOrganisations,
    },
    {
      path: '/manageusers',
      name: 'ManageUsers',
      component: ManageUsers,
    },
    {
      path: '/teams',
      name: 'ManageTeams',
      component: ManageTeams,
    },
    {
      path: '/trainers',
      name: 'ManageTrainers',
      component: ManageTrainers,
    },
    {
      path: '/manageplayers',
      name: 'ManagePlayers',
      component: ManagePlayers,
    },
    {
      path: '/livescoreboard',
      name: 'LiveScoreboard',
      component: LiveScoreboard
    },
    {
      path: '/tests',
      component: SessionsRoute,
      children: [
        {
          path: '',
          name: 'Sessions',
          component: Sessions,
        },
        {
          path: 'session/:id',
          props: true,
          name: 'SingleSession',
          component: SingleSession,
        },
      ]
    },
    {
      path: '/skillgames',
      component: DataSkillGamesRouter,
      children: [
        {
          path: '',
          name: 'DataSkillGames',
          component: DataSkillGames,

        },
        {
          path: 'achievements/:id/:name',
          props: true,
          name: 'DataSkillGamesAchievements',
          component: DataSkillGamesAchievements,
        },
      ]
    },
    {
      path: '/players',
      component: PlayersRoute,
      children: [
        {
          path: '',
          name: 'Players',
          component: Players,

        },
        {
          path: 'achievements/:id',
          props: true,
          name: 'PlayerAchievements',
          component: PlayerAchievements,
        },
        {
          path: 'achievements/skillgame/:id/:pname/:sgname/:sgid',
          props: true,
          name: 'PlayerAchievementsSkillGame',
          component: PlayerAchievementsSkillGame,
        }
      ]
    },
    {
      path: '/team',

      component: TeamRoute,
      children: [
        {
          path: '',
          name: 'Teams',
          component: Teams,

        },
        {
          path: 'achievementslist/:id',
          props: true,
          name: 'TeamAchievements',
          component: TeamAchievements,
        },
        {
          path: 'achievementslistgrid/:id',
          props: true,
          name: 'TeamAchievementsListGrid',
          component: TeamAchievementsListGrid,
        },
        {
          path: 'achievementsgrid/:id',
          props: true,
          name: 'TeamAchievementsGrid',
          component: TeamAchievementsGrid,
        }
      ]
    },
  ]
})
const openRoutes=['Login', 'Register'];
router.beforeEach((to, from, next) => { 
  if (openRoutes.includes(to.name)) 
  {
      next()
  } 
  else if(sessionStorage.getItem('token'))
  {
    next()
  }
  else {
    next('/login')
  }
})
export default router