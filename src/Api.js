import axios from 'axios'

export default () => {
  return axios.create({
    // baseURL: 'https://smartgoalstraining.com/skillgames-api-v2/'
     baseURL: 'http://laravel.fieldback.net/api/'
  })
}
